/**
 * Created by Alex on 12/2/16.
 */

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.*;
import com.google.maps.model.GeocodingResult;

import java.util.NoSuchElementException;


public class Geocoder {
    private GeoApiContext context;

    public Geocoder() {
        this.context = new GeoApiContext().setApiKey("AIzaSyDF0dEpVMiWeEc0uutNc5SLrbgH6FCJgd8");
    }

    /**
        Takes an address and returns an array that contains the latitude, longitude for that address.
        This is just a wrapper for Google's Geocode API.  The underlying result returned by the API is JSON, but
        the GeocodingResult object wraps this, so no parsing is required.
     **/
    public double[] getCoordinates(String address) {
        GeocodingApi.newRequest(this.context);
        GeocodingResult[] results = new GeocodingResult[0];
        try {
            results = GeocodingApi.geocode(context, address).await();
            System.out.println(results[0]);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("The Google Geocode request failed.");
        }
        // Check the length of the results to see if no Geocoding could be returned
        if (results.length == 0) {
            throw new NoSuchElementException();
        }
        double[] coordinates = {results[0].geometry.location.lat, results[0].geometry.location.lng};
        return coordinates;
    }

    /**
     *   Get the distance (in meters) between two addresses.  Will be used to fetch crime incidents in a
     *   certain radius.  This method is overloaded and can support either coordinate pairs or addresses
     *   @param address1: a proper street address in postcode format
     *   @param address2: a proper street address in postcode format
     */
    public double getMetricDistance(String address1, String address2) {
        double[] x = this.getCoordinates(address1);
        double[] y = this.getCoordinates(address2);
        return this.getMetricDistance(x, y);
    }

    /**
     *   Get the distance (in meters) between two coordinates.  Will be used to fetch crime incidents in a
     *   certain radius.  The Haversine Formula is used for this calculation.
     *   @param x: a double array of length two, containing latitude and longitude of an address in that order
     *   @param y: a double array of length two, containing latitude and longitude of an address in that order
     */
    public double getMetricDistance(double[] x, double[] y) {
        double deltaLat = Math.toRadians(x[0] - y[0]);
        double deltaLon = Math.toRadians(x[1] - y[1]);
        double a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(Math.toRadians(x[0])) *
                Math.cos(Math.toRadians(y[0])) * Math.pow(Math.sin(deltaLon / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = 1000 * 6373 * c;
        return d;
    }

}

import org.joda.time.LocalDateTime;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by Alex on 12/2/16.
 */
public class TestGeocoder {

    public static void main(String[] args) throws IOException {
        QueryManager manager = new QueryManager(null);
        // Get all crimes within 100 meters of this address
        long startTime = System.nanoTime();
        // TODO: DateTime constructors are going to be a pain in the ass. Find a better way if possible
        Map<Integer, Crime> results = manager.locationQuery("4000 Walnut St, Philadelphia", 100, 12, 23,
               null, new LocalDateTime(2016, 01, 01, 0, 0), LocalDateTime.now(), new String[] {"Other Assaults"});
        long endTime = System.nanoTime();
        System.out.println("Query finished in ms: " + (endTime - startTime) / 1000000);
        System.out.println("Size of returned results: " + results.size());
    }


    /**
     * Test the speed and reliability of geocoding every Crime instance in one go as the csv file of 1000000 > crimes
     * is read in.  Print runtime of this reading process.
     *
     * NOTE: Results of test indicate that this strategy is not feasible. We are limited to 10 geocode requests per
     * second by Google's API.  Our set has over 1 million instances, so that would take ~1.5 days to geocode every
     * instance
     */
    private static void largeScaleGeocoding() {
        // The file reading occurs on instantiation
        QueryManager manager = new QueryManager("src/PPD_Crime_Incidents_2006-Present.csv");
    }


    /**
     * A few simple, informal tests for the Geocoder (parsing misspelled addresses, measuring distance, etc.).  Results
     * are printed to the console
     */
    private static void simpleGeocodingTests() {
        Geocoder myGeocoder = new Geocoder();
        String hillHouseAddress = "3333 Walnut St, Philadelphia";
        String huntsmanAddress = "Huntsman Hall, Philadelphia";
        double[] hillHouse = myGeocoder.getCoordinates(hillHouseAddress);
        double[] hunstmanHall = myGeocoder.getCoordinates(huntsmanAddress);
        double distance = myGeocoder.getMetricDistance(hillHouse, hunstmanHall);
        System.out.println(Arrays.toString(hillHouse));
        System.out.println(Arrays.toString(hunstmanHall));
        System.out.println(distance);

        // Now try it directly with the addresses
        System.out.println(myGeocoder.getMetricDistance(huntsmanAddress, hillHouseAddress));
        System.out.println(myGeocoder.getMetricDistance(hillHouseAddress, huntsmanAddress));

        // Our database is full of misspelled locations.  Try a hard one here and see if it gives correct results
        String misspelledIntersectionAddress = "38TH ST / MARKETUT ST";
        String correctIntersectionAddress = "38TH ST / MARKET ST";
        double[] misspelledIntersection = myGeocoder.getCoordinates(misspelledIntersectionAddress);
        double[] correctIntersection = myGeocoder.getCoordinates(correctIntersectionAddress);
        System.out.println(Arrays.toString(misspelledIntersection));
        System.out.println(Arrays.toString(correctIntersection));
    }

}

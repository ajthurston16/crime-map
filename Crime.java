import org.joda.time.LocalDateTime;

/**
 * Created by Alex on 12/3/16.
 *
 * This class creates represents a single crime, storing the relevant information (location, type of crime, etc.)
 */
public class Crime {
    private int id;
    private int hour;
    private String crimeCategory;
    private double lat;
    private double lng;
    private LocalDateTime dateTime;
    private String blockAddress;



    public Crime(int id, String dateTime, int hour, String code, double lat, double lng, String blockAddress) {
        this.id = id;
        this.hour = hour;
        this.crimeCategory = code;
        this.lat = lat;
        this.lng = lng;
        this.dateTime = new LocalDateTime(dateTime);
        this.blockAddress = blockAddress;
    }

    public String getBlockAddress() {
        return blockAddress;
    }

    // Getters for all relevant fields
    public double getLat() {
        return lat;
    }

    public int getId() {
        return id;
    }

    public int getHour() {
        return hour;
    }

    public String getCrimeCategory() {
        return crimeCategory;
    }

    public double getLng() {
        return lng;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}

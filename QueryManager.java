import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.joda.time.LocalDateTime;

import java.net.URLEncoder;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
/**
 * Created by Alex on 12/3/16.
 *
 * This class handles the the high level data processing and queries.  It will be the main interface between the
 * front and backends
 */
public class QueryManager implements Queryer {

    private String infile;
    private Geocoder geocoder;

    public QueryManager(String infile) {
        this.geocoder = new Geocoder();
    }


    /**
     * Read the data from the filenames into data structures, so they can be manipulated.
     * Ratings file parses info to create User objects that are stored in the UsersCollection object
     * Movies file is parsed into movie IDs and titles, which are simply stored as key, value pairs in a hash map.
     * While doing this, calculate the overall mean rating.
     */
    private void readData() {
        try {
            long startTime = System.nanoTime();

            // First, read the file of users and their ratings
            FileReader crimeReader = new FileReader(this.infile);
            BufferedReader buffie = new BufferedReader(crimeReader);
            Geocoder geocoder = new Geocoder();
            // Set initial capacity of our hashMap to be one million (rough size of crime set)
            Map<String, Crime> allCrimes = new HashMap<String, Crime>(1000000);
            Set<String> allAddressBlocks = new HashSet<String>();
            int instanceCount = 0;

            // Skip the header on the first line
            buffie.readLine();
            String rawRow = buffie.readLine();

            // Unpack each row into the userID, movieID, and the user's rating. Ignore the time stamp
            while (rawRow != null) {
                String[] rowArray = rawRow.split(",");
                String addressBlock = rowArray[7];

                // Do the actual geocoding immediately, then create a new crime instance with info
//                double[] coordinates  = geocoder.getCoordinates(addressBlock);
//                Crime crime = new Crime(rowArray, coordinates);
//                String hashKey = Double.toString(coordinates[0]);
//                allCrimes.put(hashKey, crime);

                // Place the address block in a set so we can get the unique number of address blocks
                allAddressBlocks.add(addressBlock);
                instanceCount++;
                // Read the next line of the file
                rawRow = buffie.readLine();

            }
            buffie.close();
            long endTime = System.nanoTime();
            System.out.println("Seconds to complete reading file: " + (endTime - startTime) / 1000000000);
            System.out.println("Number of crime instances = " + instanceCount);
            System.out.println("Number of unique address blocks = " + allAddressBlocks.size());
            // Throw an exception if the correct file is not found. Kill the program to prevent corrupting data structures
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found. Verify that your file is in the correct path, then try reloading "
                    + "the program.");
        } catch (IOException e) {
            System.out.println("IO Exception. This is likely due to malformed files. Check the format and "
                    + "try reloading the program.");
        }
    }

    /**
     * Sends a request to the Crime Incidents API, then parses the resulting JSON into crime instances
     * @param url : URL object for the query we want to make
     * @return an array of JSON objects representing the return value of the query
     */
    private Map<Integer, Crime> getResultsSet(URL url) throws IOException {
        BufferedReader buffie = new BufferedReader(new InputStreamReader(url.openStream()));
        JsonParser parser = new JsonParser();
        Map<Integer, Crime> crimeMap = new HashMap<>(50);

        // The parser gives us an array of JsonElements, each of which represents a crime instance
        JsonArray asArray = parser.parse(buffie).getAsJsonArray();

        for (JsonElement currElt : asArray) {
            // Cast each JsonElement down to a JsonObject, which can be searched like a map or dict
            JsonObject asObj = (JsonObject) currElt;

            // Get the standard fields that we need for to build the Crime object
            String datetime = asObj.get("dispatch_date_time").getAsString();
            String code = asObj.get("text_general_code").getAsString();
            String blockAddress = asObj.get("location_block").getAsString();
            int hour = asObj.get("hour").getAsInt();
            int id = Integer.parseInt(asObj.get("dc_key").getAsString().substring(1, 10));

            // Get a JsonArray for the coordinates, then pull the longitude and latitude out of this
            JsonArray coordinates = asObj.get("shape").getAsJsonObject().get("coordinates").getAsJsonArray();
            double lng = coordinates.get(0).getAsDouble();
            double lat = coordinates.get(1).getAsDouble();

            // Create the actual Crime object and place it in a map to be returned
            Crime c = new Crime(id, datetime, hour, code, lat, lng, blockAddress);
            crimeMap.put(id, c);
        }
        return crimeMap;
    }


    /**
     * Get the set of all crimes in a specified vicinity of the passed address. Wraps the getResultsSet and
     * constructURLMethods.  This method is overloaded to support both coordinates and proper addresses
     * @param address: A postcode formatted address
     * @param vicinity: An int representing the radius from the center to search within
     * @param vicinity radius of search circle
     * @param fromHour the start of the hour range (0-23). -1 if unused filter
     * @param toHour the end of the hour range (0-23). -1 if unused filter (if set toHour = fromHour if not a range)
     * @param crimeCode and int array of length >= 1 of the general categories of the crimes. null if unused filter
     * @param fromDateTime starting datetime, null if unused filter
     * @param toDateTime ending datetime, null if unused filter
     * @return A map of integer IDs to Crime objects matching the queried results
     */
    public Map<Integer, Crime> locationQuery(String address, int vicinity, int fromHour, int toHour, int[] crimeCode,
                                             LocalDateTime fromDateTime, LocalDateTime toDateTime,
                                             String[] crimeCategory) {
        double[] coordinates = this.geocoder.getCoordinates(address);
        double lat = coordinates[0];
        double lng = coordinates[1];
        // Call the other version of this method to do the majority of the work
        return this.locationQuery(lat, lng, vicinity, fromHour, toHour, crimeCode, fromDateTime, toDateTime,
                crimeCategory);
    }


    /**
     * From coordinates and a vicinity, return a map of all crimes that occurred within inside the radius of a circle
     * centered around those coordinates.
     * @param lat: Latitude
     * @param lng: Longitude
     * @param vicinity: An integer representing the radius of the search circle in meters
     * @param fromHour the start of the hour range (0-23). -1 if unused filter
     * @param  toHour the end of the hour range (0-23). -1 if unused filter (set toHour = fromHour if not a range)
     * @param crimeCode and int array of length >= 1 of the general categories of the crimes. null if unused filter
     * @param fromDateTime starting datetime, null if unused filter
     * @param toDateTime ending datetime, null if unused filter
     * @param  crimeCategory a string array whose elements are the strings that corresponds to a crimeCodes
     *                       (e.g. Other Assaults for crimeCode = 800). This param is ignored if crimeCode != null
     * @return a map of unique integer IDs to Crime instances
     */
    public Map<Integer, Crime> locationQuery(double lat, double lng, int vicinity, int fromHour, int toHour, int[]
                                             crimeCode, LocalDateTime fromDateTime, LocalDateTime toDateTime, String[]
                                             crimeCategory) {
        Map<Integer, Crime> result = null;
        try {
            URL url = this.constructQueryURL(lat, lng, vicinity, fromHour, toHour, crimeCode, fromDateTime,
                    toDateTime, crimeCategory);
            result = this.getResultsSet(url);

        }
        catch (IOException e) {
            //TODO: Determine if there is not a more appropriate place to handle the exceptions (maybe it should be
            //on the front end? Or maybe even farther down the stack?)
            e.printStackTrace();
        }
        return result;
    }



    /**
     * Given a set of parameters, construct the URL that will be sent as a query to the Crime Incidents API
     * @param lat   latitude
     * @param lng   longitude
     * @param vicinity radius of search circle
     * @param fromHour the start of the hour range (0-23). -1 if unused filter
     * @param  toHour the end of the hour range (0-23). -1 if unused filter (set toHour = fromHour if not a range)
     * @param ucr the general Uniform Crime Reporting code category. -1 if unused filter
     * @param fromDateTime starting datetime, null if unused filter
     * @param toDateTime ending datetime, null if unused filter
     * @return an encoded URL to query a SQL-like database (uses "SoQL" syntax)
     */
    private URL constructQueryURL(double lat, double lng, int vicinity, int fromHour, int toHour, int[] ucr,
                                  LocalDateTime fromDateTime, LocalDateTime toDateTime, String[] crimeCategory)
            throws MalformedURLException, UnsupportedEncodingException {
        //TODO: If we somehow make queries asynchronous, StringBuffer will be safer than StringBuilder
        String base = "https://data.phila.gov/resource/sspu-uyfa.json?$where=";
        StringBuilder buildStr = new StringBuilder();
        if (lat != 0 && lng != 0) {
            buildStr.append("within_circle(shape,").append(lat).append(",").append(lng).append(",")
                    .append(vicinity).append(")");
        }
        if (fromHour != -1 && toHour != -1) {
            buildStr.append(" and hour between ").append(fromHour).append( " and ").append(toHour);
        }
        // Could have more than one crime code that we are filtering by, so we pass in an array
        if (ucr != null) {
            buildStr.append(" and ucr_general in(");
            // The syntax is very tricky here. ucr_general is a text param, so needs to be formatted as String literal
            String comma = "";
            for (int i = 0; i < ucr.length; i++) {
                buildStr.append(comma).append("'").append(ucr[i]).append("'");
                comma = ",";
            }
            buildStr.append(")");
        }
        // DateTimes are in an ISO format, so the Java object and the SQL query represent them w/ same string literal
        if (fromDateTime != null && toDateTime != null) {
            buildStr.append(" and dispatch_date_time between ").append("'").append(fromDateTime.toString()).append("'");
            buildStr.append(" and ").append("'").append(toDateTime.toString()).append("'");
        }
        if (ucr == null && crimeCategory != null) {
            buildStr.append(" and text_general_code in(");
            // Syntax is much like ucr_general
            String comma = "";
            for (int i = 0; i < crimeCategory.length; i++) {
                buildStr.append(comma).append("'").append(crimeCategory[i]).append("'");
                comma = ",";
            }
            buildStr.append(")");
        }

        //TODO: Add support for text_general_code (text representation of ucr_code)
        // Encode the URL for safety
        // Note: encoding any part of the base URL (especially "$where=") will cause the entire query to fail
        String query = URLEncoder.encode(buildStr.toString(), "UTF-8");
        query = base + query;
        System.out.println("Your encoded URL parameters: " + query);
        return new URL(query);
    }
}

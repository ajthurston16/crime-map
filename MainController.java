package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class MainController implements Initializable {

	String address = "philadelphia";
	double latitude = 39.9526;
	double longitude = -75.1652;
	int zoom = 10;
	String key = "AIzaSyByu3CeuS-ZrNbcTjAF7IO-tBCHCzGGuWs";
	String markers = "&markers=size:tiny%7Ccolor:blue%7C62.107733,-145.541936";
	
	
	@FXML
	private TextField addressinput;
	@FXML private WebView webView;
	private WebEngine engine;
	@FXML
	public ComboBox<String> time;
	@FXML
	public ComboBox<String> type;
	
	ObservableList<String> timeSelection = FXCollections.observableArrayList("daytime", "night", "all day");
	ObservableList<String> typeSelection = FXCollections.observableArrayList("homicide", "robbery", "rapes", "aggrevated assults","burglaries", "theft", "all");
	String selectedTime = "all day";
	String selectedType = "all";
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		time.setItems(timeSelection);
		type.setItems(typeSelection);
		
		engine = webView.getEngine();
		engine.load("https://maps.googleapis.com/maps/api/staticmap?"
				+ "center="+ latitude +  ","+ longitude
				+ "&zoom=" + zoom + "&size=500x500&maptype=roadmap" 
				+ "&key="
				+ key);
	}
	
	public void getTime(ActionEvent event) throws Exception{
		selectedTime = time.getSelectionModel().getSelectedItem().toString();
	}
	
	public void getType(ActionEvent event) throws Exception{
		selectedType = type.getSelectionModel().getSelectedItem().toString();
	}
	
	public void getAddressAndConvert(ActionEvent event) throws Exception{
		address = addressinput.getText();

		System.out.println(address + " " + selectedTime + " " + selectedType);
		ArrayList<double[]> test = new ArrayList<>();
		double[] d = getAddress();
		double r1 = Math. random() * 0.014 - 0.014; 
		double r2 = Math. random() * 0.014 - 0; 
		double r3 = Math. random() * 0.014 - 0.014; 
		double r4 = Math. random() * 0.014 - 0; 
		double r5 = Math. random() * 0.014 - 0.014;
		double r6 = Math. random() * 0.014 - 0.014; 
		double r7 = Math. random() * 0.014 - 0; 
		double r8 = Math. random() * 0.014 - 0.014; 
		double r9 = Math. random() * 0.014 - 0; 
		double r10 = Math. random() * 0.014 - 0.014; 
		double[] d1 = {d[0]+r1, d[1]+r2};
		double[] d2 = {d[0]+r2, d[1]+r3};
		double[] d3 = {d[0]+r3, d[1]+r4};
		double[] d4 = {d[0]+r4, d[1]+r5};
		double[] d5 = {d[0]+r5, d[1]+r1};
		double[] d6 = {d[0]+r6, d[1]+r7};
		double[] d7 = {d[0]+r8, d[1]+r9};
		double[] d8 = {d[0]+r9, d[1]+r10};
		double[] d9 = {d[0]+r6, d[1]+r1};
		double[] d10 = {d[0]+r10, d[1]+r4};
		test.add(d1);
		test.add(d2);
		test.add(d3);
		test.add(d4);
		test.add(d5);
		test.add(d6);
		test.add(d7);
		test.add(d8);
		test.add(d9);
		test.add(d10);
		
		addMarkers(d, test);
	}
	
	public String getMarkerList(ArrayList<double[]> list) {
		String results = "";
		for (double[] d : list) {
			results += "&markers=size:mid%7Ccolor:red%7C" + d[0] + "," + d[1];
		}
		return results;
	}
	
	public double[] getAddress(){
		double r1 = Math.random() * 0.1 - 0.1; 
		double[] test = {39.9526+r1, -75.1652+r1};
		return test;
	}
	public void addMarkers(double[]d, ArrayList<double[]> list) {
		engine.load("https://maps.googleapis.com/maps/api/staticmap?"
				+ "center="+d[0] +  ","+  d[1]
				+ "&zoom=14&size=500x500" + getMarkerList(list) + "&maptype=satellite" 
				+ "&key="
				+ key);
	}
	
}

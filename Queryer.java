import org.joda.time.LocalDateTime;
import java.util.Map;

/**
 * Created by Alex
 *
 * This interface exists to link the frontend graphics to the backend SQL queries that generate search results.
 * The frontend interacts with this through one (overloaded) method: this provides the capability to search for results with
 * any combination of time, location, and category filters (or no filters at all)
 */
public interface Queryer {


    /**
     * Get the set of all crimes in a specified vicinity of the passed address.
     * @param address: A postcode formatted address
     * @param vicinity: An int representing the radius from the center to search within
     * @param vicinity radius of search circle
     * @param fromHour the start of the hour range (0-23). -1 if unused filter
     * @param toHour the end of the hour range (0-23). -1 if unused filter (if set toHour = fromHour if not a range)
     * @param crimeCode and int array of length >= 1 of the general categories of the crimes. null if unused filter
     * @param fromDateTime starting datetime, null if unused filter
     * @param toDateTime ending datetime, null if unused filter
     * @return A map of integer IDs to Crime objects matching the queried results
     */
    public Map<Integer, Crime> locationQuery(String address, int vicinity, int fromHour, int toHour, int[]
            crimeCode, LocalDateTime fromDateTime, LocalDateTime toDateTime, String[] crimeCategory);



    /**
     * From coordinates and a vicinity, return a map of all crimes that occurred within inside the radius of a circle
     * centered around those coordinates.
     * @param lat: Latitude
     * @param lng: Longitude
     * @param vicinity: An integer representing the radius of the search circle in meters
     * @param fromHour the start of the hour range (0-23). -1 if unused filter
     * @param  toHour the end of the hour range (0-23). -1 if unused filter (set toHour = fromHour if not a range)
     * @param crimeCode and int array of length >= 1 of the general categories of the crimes. null if unused filter
     * @param fromDateTime starting datetime, null if unused filter
     * @param toDateTime ending datetime, null if unused filter
     * @param  crimeCategory a string array whose elements are the strings that corresponds to a crimeCodes
     *                       (e.g. Other Assaults for crimeCode = 800). This param is ignored if crimeCode != null
     * @return a map of unique integer IDs to Crime instances
     */
    public Map<Integer, Crime> locationQuery(double lat, double lng, int vicinity, int fromHour, int toHour, int[]
            crimeCode, LocalDateTime fromDateTime, LocalDateTime toDateTime, String[]
                                                     crimeCategory);
}
